import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Scanner;
import java.io.File;
import java.util.Arrays;

import java.io.IOException;

import java.net.Socket;
import java.net.ServerSocket;

public class Server extends FiveInARow {
	private static int countMoves = 0;
	private static int winner = 0;
	private static boolean win = false;
	private static final String nullString = "";
	private static String configFile = "config.txt";
	private static String[][] board = null;
	private static Player[] clients = null;
	private static ServerSocket listener;
	private static int MIN_PLAYERS_BEFORE_LISTEN = 2; // if num active players < ~ : server goes back into listen mode to get more players
	private static int MIN_PLAYERS_BEFORE_KICKALL = 1; // if num active players < ~ : server kicks all of them and goes into listen mode

	public static void main(String[] args) {
		parseConfig();

		int id;
		int i = 0;
		Socket clientConnection = null;
		clients = new Player[NUM_PLAYERS];
		board = new String[HEIGHT+2][WIDTH+2];
		for (i = 0; i < NUM_PLAYERS; i++) {
			clients[i] = new Player(i);
			clients[i].disconnected = true;
		}

		try{
			listener = new ServerSocket(port);
		} catch(Exception e) { System.out.println("Failed to create server socket."); System.exit(1); }

		try {
			listener.setSoTimeout(0); // accept() blocks indefinitely
		} catch(Exception e) { System.out.println("Failed to set timeout to 0."); }
	
		while(true){ // until server is force closed
			// make a "pretend" board on the server-side (reset board)
			for(int h = 0; h < HEIGHT+2; h++){ // otherwise checking for win is a nightmare of try-catches
				for(int w = 0; w < WIDTH+2; w++){
					board[h][w] = nullString;
				}
			}

			if (activePlayers() < MIN_PLAYERS_BEFORE_LISTEN) {
				System.out.println("Listening for clients....");
				for(i = 0; i < NUM_PLAYERS; i++){ // get players
					if (clients[i] != null && !clients[i].disconnected) continue;

					try {
						clientConnection = listener.accept();
					} catch(Exception e) { System.out.println("Failed to accept client connection."); i--; continue; }

					clients[i].openDataStreams(clientConnection);
					clients[i].clear();

					System.out.println("Established connection with "+clientConnection.getInetAddress().toString());

					try {
						clients[i].out.writeUTF(""+HEIGHT+" "+WIDTH+" "+IN_A_ROW+" "+ALLOW_DISLOCATION+" "+NUM_PLAYERS+" "+clients[i].ID); //publish to player - game config
					} catch(Exception e) { System.out.println("Failed to send config to player "+i); i--; continue; }
				}
			}

			String boardAsLine = boardToLine();

			
			for(i = 0; i < NUM_PLAYERS; i++) {
				if (!clients[i].disconnected) {
					try {
						clients[i].out.writeUTF(""+clients[i].turn+" "+boardAsLine); //publish to player - his number & initial board layout
					} catch(Exception e) { System.out.println("Failed to send player turn# & initial board layout to clients."); clients[i].disconnected = true; }
				}
			}
			System.out.println("Now playing with "+(activePlayers()-1)+" other players:");
			//System.out.println("The lineup: ");
			for (Player pl : clients) {
				System.out.println(pl.mark+" - discoinnected: "+pl.disconnected);
			}

			gameRound();
			//System.out.println("wiener was "+clients[winner].mark);
			// adjust player order -> first player = now penultimate, winner goes last
			if (winner == 0) { // if winner was the first player
				for (i = 1; i < NUM_PLAYERS; i++) 
					clients[i].turn -= 1;
				clients[0].turn = NUM_PLAYERS-1;
			} else { // the winner was not the first player
				for (i = 1; i < NUM_PLAYERS; i++) {
					if(clients[i].turn == winner) winner -= 1;
					clients[i].turn -= 1;
				}
				clients[0].turn = NUM_PLAYERS-1;
				for (i = 0; i < NUM_PLAYERS; i++) {
					if(clients[i].turn > winner)
						clients[i].turn -= 1;
					else if (clients[i].turn == winner)
						clients[i].turn = NUM_PLAYERS-1;
				}
			}
			Arrays.sort(clients);

			System.out.println();
		}
	}
	
	// returns how many players were lost during the game
	public static void gameRound() {
		win = false;
		countMoves = 0;
		int i = 0;

		//System.out.println("Game on!!");
		
		while(!win && countMoves < WIDTH*HEIGHT){ // game on!
			for(i = 0; i < NUM_PLAYERS; i++){
				handlePlayerTurn(clients[i]);
				countMoves++;
				if(win || countMoves >= WIDTH*HEIGHT) break;
			}
		}
		
		System.out.println("Game ended");

		int n_continues = 0;
		// get player continues
		for(i = 0; i < NUM_PLAYERS; i++){
			if (!clients[i].disconnected) {
				try {
					if (clients[i].in.readBoolean()) n_continues++;
					else {
						clients[i].closeDataStreams();
						clients[i].socket.close();
						System.out.println("Connection with "+clients[i].socket.getInetAddress().toString()+" terminated");
					}
				} catch(Exception e) { System.out.println("Failed to read continuation status or close client socket."); clients[i].disconnected = true; }
			}
		}

		boolean continueGame;
		if (n_continues < MIN_PLAYERS_BEFORE_KICKALL) // lost some players along the way, will not continue if the num of players is below the specified minimum
			continueGame = false;
		else 
			continueGame = true;

		for(i = 0; i < NUM_PLAYERS; i++){
			if (!clients[i].disconnected) {
				try {
					clients[i].out.writeBoolean(continueGame);
				} catch(Exception e) { System.out.println("Failed to send continuation status."); clients[i].disconnected = true; }
			}
		}
	}

	protected static void handlePlayerTurn(Player p) {
		int x = 0;
		int y = 0;
		int i = 0;
		int wiener = 0;
		String response = "";
		String winDir = "";

		if (!p.disconnected) {
			while(true){
				try { // get coords
					response = p.in.readUTF();
				} catch(Exception e) { System.out.println("Failed to read coords from client."); p.disconnected = true;}
				if(p.disconnected) break;

				Scanner scn = new Scanner(response);
				x = scn.nextInt();
				y = scn.nextInt();
				
				//check for errors
				/*
				errors: 
					- not your turn -> 0 // this can't happen...
					- dislocated mark (not bound to previous group) -> 1
					- mark already exists at location -> 2
				*/
				try {
					if(!board[y][x].equals(nullString)){
						p.out.writeUTF("error 2");
						continue;
					}
					if((!ALLOW_DISLOCATION && !boardIsClear()) && !hasNeighbours(x,y)){
						p.out.writeUTF("error 1");
						continue;
					}
				} catch(Exception e) { System.out.println("Failed to send error message to client."); p.disconnected = true;}
				break;
			}
		}

		if (!p.disconnected) {
			try {
				p.out.writeUTF("no errors");
			} catch(Exception e) { System.out.println("Failed to confirm sent coordinates."); p.disconnected = true;}
		}
		
		if (!p.disconnected) { // if we at least got a valid set of coords from player
			//add mark to board
			board[y][x] = p.mark;
			
			//check for winning condition
			winDir = checkWin(p.mark,x,y);
			if(!winDir.equals("")){
				win = true;
			}

			wiener = p.ID;
			if (winDir.equals("a")) {
				for (i = 0; i < NUM_PLAYERS; i++) {
					if (!clients[i].disconnected) {
						wiener = clients[i].ID;
						break;
					}
				}
			}
		}
			
		// respond with new board layout & win/lose condition
		String boardAsLine = boardToLine();

		//System.out.println(p.mark+"s turn");
		//System.out.println("coordsError "+p.disconnected);
		//publish to everybody
		for(i = 0; i < NUM_PLAYERS; i++){
			if (!clients[i].disconnected) {
				try {
					//System.out.println("sending board");
					clients[i].out.writeUTF(boardAsLine);
					//System.out.println(boardAsLine);
					//System.out.println("to "+clients[i].mark);

					if(!win){
						if(countMoves+1 >= WIDTH*HEIGHT){ // tie occured
							clients[i].out.writeUTF("full");
						}else{
							clients[i].out.writeUTF("normal");
						}
					}else{
						if(clients[i].ID != wiener) {
							clients[i].out.writeUTF("lose");
						}else{
							clients[i].out.writeUTF("win "+winDir);
							winner = clients[i].turn;
						}
					}
				} catch(Exception e) { System.out.println("Failed to send board layout and/or game status to clients."); clients[i].disconnected = true;}
			}
		}
	}
	
	private static int activePlayers() {
		int n = NUM_PLAYERS;
		for (Player p : clients) {
			if (p == null || p.disconnected) n--;
		}
		return n;
	}

	private static boolean hasNeighbours(int x, int y){
		if(!board[y-1][x-1].equals(nullString)) return true;
		if(!board[y-1][x].equals(nullString)) return true;
		if(!board[y-1][x+1].equals(nullString)) return true;
		if(!board[y][x+1].equals(nullString)) return true;
		if(!board[y+1][x+1].equals(nullString)) return true;
		if(!board[y+1][x].equals(nullString)) return true;
		if(!board[y+1][x-1].equals(nullString)) return true;
		if(!board[y][x-1].equals(nullString)) return true;
		
		return false;
	}

	private static boolean boardIsClear() {
		for (String[] row : board) {
			for (String cell : row) {
				if (!cell.equals(nullString))
					return false;
			}
		}
		return true;
	}
	
	// returns true if mark at (x,y) forms a line of IN_A_ROW same marks
	private static String checkWin(String mark, int xCoord, int yCoord){
		/*
		The 4+1 lines of victory 
			| v  vertical
			- h  horizontal
			/ du diagonal up
			\ dd diagonal down
			a auto-win (all other players left the game)
		*/
		// check v
		int inarow = 1;
		int x = xCoord;
		int y = yCoord;
		while(board[y+1][x].equals(mark)){
			inarow++;
			y++;
		}
		y = yCoord;
		while(board[y-1][x].equals(mark)){
			inarow++;
			y--;
		}
		if(inarow >= IN_A_ROW){
			return "v";
		}

		// check h
		inarow = 1;
		x = xCoord;
		y = yCoord;
		while(board[y][x+1].equals(mark)){
			inarow++;
			x++;
		}
		x = xCoord;
		while(board[y][x-1].equals(mark)){
			inarow++;
			x--;
		}
		if(inarow >= IN_A_ROW){
			return "h";
		}

		// check du
		inarow = 1;
		x = xCoord;
		y = yCoord;
		while(board[y+1][x+1].equals(mark)){
			inarow++;
			x++;
			y++;
		}
		x = xCoord;
		y = yCoord;
		while(board[y-1][x-1].equals(mark)){
			inarow++;
			x--;
			y--;
		}
		if(inarow >= IN_A_ROW){
			return "u";
		}

		// check dd
		inarow = 1;
		x = xCoord;
		y = yCoord;
		while(board[y-1][x+1].equals(mark)){
			inarow++;
			x++;
			y--;
		}
		x = xCoord;
		y = yCoord;
		while(board[y+1][x-1].equals(mark)){
			inarow++;
			x--;
			y++;
		}
		if(inarow >= IN_A_ROW){
			return "d";
		}
		
		//check a
		if (activePlayers() == 1) {
			return "a";
		}

		return "";
	}
	
	private static String boardToLine(){
		String boardAsLine = "";
		for(int i = 1; i <= HEIGHT; i++){
			for(int j = 1; j <= WIDTH; j++){
				if(board[i][j].equals(nullString)){
					boardAsLine += ".";
				}else{
					boardAsLine += board[i][j];
				}
			}
		}
		return boardAsLine;
	}

	private static void parseConfig() {
		System.out.println("Parsing config file...");
		Scanner scn = null;
		try {
			scn = new Scanner(new File(configFile));
		} catch (Exception e) {
			System.out.println("Unable to parse config, using defaults.");
		}

		String line;
		String param;
		String val;
		String[] banana;
		while(scn.hasNextLine()) {
			line = scn.nextLine();
			if (line.equals("") || line.charAt(0) == '#') continue;
			banana = line.split(":");
			param = banana[0].trim();
			val = banana[1].trim();

			if (param.equals("height")) {
				try {
					HEIGHT = Integer.parseInt(val);
					System.out.println("HEIGHT = "+HEIGHT);
				} catch (Exception e) {
					System.out.println("Error parsing HEIGHT");
				}
			}
			else if (param.equals("width")) {
				try {
					WIDTH = Integer.parseInt(val);
					System.out.println("WIDTH = "+WIDTH);
				} catch (Exception e) {
					System.out.println("Error parsing WIDTH");
				}
			}
			else if (param.equals("inARow")) {
				try {
					IN_A_ROW = Integer.parseInt(val);
					System.out.println("IN_A_ROW = "+IN_A_ROW);
				} catch (Exception e) {
					System.out.println("Error parsing IN_A_ROW");
				}
			}
			else if (param.equals("allowDislocation")) {
				try {
					int bool = Integer.parseInt(val);
					if(bool == 0) ALLOW_DISLOCATION = false;
					else ALLOW_DISLOCATION = true;
					System.out.println("ALLOW_DISLOCATION = "+ALLOW_DISLOCATION);
				} catch (Exception e) {
					System.out.println("Error parsing ALLOW_DISLOCATION");
				}
			}
			else if (param.equals("numPlayers")) {
				try {
					NUM_PLAYERS = Integer.parseInt(val);
					System.out.println("NUM_PLAYERS = "+NUM_PLAYERS);
				} catch (Exception e) {
					System.out.println("Error parsing NUM_PLAYERS");
				}
			}
			else if (param.equals("minPlayersInGame")) {
				try {
					MIN_PLAYERS_BEFORE_LISTEN = Integer.parseInt(val);
					if (MIN_PLAYERS_BEFORE_LISTEN > NUM_PLAYERS) MIN_PLAYERS_BEFORE_LISTEN = NUM_PLAYERS;
					System.out.println("MIN_PLAYERS_BEFORE_LISTEN = "+MIN_PLAYERS_BEFORE_LISTEN);
				} catch (Exception e) {
					System.out.println("Error parsing MIN_PLAYERS_BEFORE_LISTEN");
				}
			}
			else if (param.equals("minPlayersContinue")) {
				try {
					MIN_PLAYERS_BEFORE_KICKALL = Integer.parseInt(val);
					if (MIN_PLAYERS_BEFORE_KICKALL > NUM_PLAYERS) MIN_PLAYERS_BEFORE_KICKALL = NUM_PLAYERS;
					if (MIN_PLAYERS_BEFORE_KICKALL < 1) MIN_PLAYERS_BEFORE_KICKALL = 1;
					System.out.println("MIN_PLAYERS_BEFORE_KICKALL = "+MIN_PLAYERS_BEFORE_KICKALL);
				} catch (Exception e) {
					System.out.println("Error parsing MIN_PLAYERS_BEFORE_KICKALL");
				}
			}
		}

		System.out.println("Done.");
	}
}