Originally intended to be a multiplayer five in a row game, but supports 
anything from tic-tac-toe (three in a row) onwards.

Ant is required to automatically build. Run:
$ ant
from the same directory that build.xml is in. The runnable should be  
dist/Server.jar and dist/Client.jar

To manually build, compile Server.java and Client.java with the javac command. 
You can then play the game by typing
$ java Server
and
$ java Client

To play, first run the Server.jar, preferably from a console with
$ java -jar Server.jar
because it has no GUI. A valid config.txt must also be in the same directory as 
the jar file or the server wont start.
When it has started listening for clients you can run Client.jar and type in the 
address of the server. Do note that the server and clients currently have to be 
on the same local area network. 
Once the specified quota of players (clients) have joined, the server will begin 
the game.
The goal is to get five of your marks (or however many was set in the config) in 
a row and to prevent other players from doing so.

All parameters can be adjusted in config.txt. Default five in a row and 
tic-tac-toe configs are provided as an example. 
For the new config to take effect you need to restart the server and reconnect 
all the clients.