
public abstract class FiveInARow {
	
	// Global vars used by client & server
	// Player symbols - that is names of gif files in ./marks/ (has to be 1 char long)
	protected final static String[] PLAYER_ID = {"X", "O", "S", "H", "Q"};
	// Server details
	protected static int port = 1234;
	protected static String host = "localhost"; // initial host; can be changed in-game (or rather must be confirmed at least)
	// Initial button dimensions
	protected final static int INIT_BTTN_W = 25;
	protected final static int INIT_BTTN_H = INIT_BTTN_W;

	// values get read by server from conf file and sent to clients
	protected static int HEIGHT = 20;
	protected static int WIDTH = 20;
	protected static int IN_A_ROW = 5;
	protected static boolean ALLOW_DISLOCATION = false;
	protected static int NUM_PLAYERS = 2;
	
}