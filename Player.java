import java.net.Socket;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Comparator;
import java.lang.Comparable;

class Player extends FiveInARow implements Comparator, Comparable {
	public int ID;
	public String mark;
	public Socket socket;
	public int turn;
	public DataInputStream in;
	public DataOutputStream out;
	public int wins;
	public boolean disconnected;
	
	public Player(int i) {
		ID = i;
		mark = PLAYER_ID[ID];
		turn = i;
	}

	public Player(int ID, Socket socket, int turn) {
		this.ID = ID;
		this.socket = socket;
		mark = PLAYER_ID[ID];
		this.turn = turn;
		try {
			in = new DataInputStream(socket.getInputStream());
			out = new DataOutputStream(socket.getOutputStream());
		} catch (Exception e) {
			System.out.println("cannot get data streams for player "+ID+";\n"+e);
		}
		wins = 0;
		disconnected = false;
	}

	public void openDataStreams(Socket socket) {
		closeDataStreams();

		this.socket = socket;
		try {
			in = new DataInputStream(socket.getInputStream());
			out = new DataOutputStream(socket.getOutputStream());
		} catch (Exception e) {
			System.out.println("Cannot open data streams for player "+ID+";\n"+e);
		}
	}

	public void closeDataStreams() {
		try {
			if (in != null) in.close();
			if (out != null) out.close();
		} catch (Exception e) {
			System.out.println("Cannot close data streams for player "+ID+";\n"+e);
		}
	}

	public void setID(int id) {
		this.ID = id;
		mark = PLAYER_ID[this.ID];
	}

	public void clear() {
		wins = 0;
		disconnected = false;
	}

	public int compareTo(Object p) {
		if (!(p instanceof Player)) return 0;
		return turn-((Player)p).turn;
	}

	public int compare(Object p1, Object p2) {
		if (!(p1 instanceof Player) || !(p2 instanceof Player)) return 0;
		return ((Player)p1).turn-((Player)p2).turn;
	}

	public boolean equals(Object p) {
		if (!(p instanceof Player)) return false;
		return ((Player)p).turn == turn;
	}
}