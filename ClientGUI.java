import java.lang.Runnable;
import javax.swing.SwingUtilities;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JToggleButton;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.GridLayout;
import java.awt.BorderLayout;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Point;

import javax.swing.AbstractAction;
import javax.swing.Action;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.awt.event.InputEvent.CTRL_DOWN_MASK;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;

import javax.swing.UIManager;

public class ClientGUI extends Client {
	
	private JToggleButton[][] toggleButtons = null;
	private boolean actionFired = false;
	private boolean connectFired = false;
	private int xCoord;
	private int yCoord;	
	private static final Color WIN_COLOR = new Color(255, 215, 115);
	private JLabel statusbar = new JLabel(" ");
	private JMenuBar menubar = new JMenuBar();
	private JFrame frame = null;
	
	// Constructor
	public ClientGUI(){
		toggleButtons = new JToggleButton[HEIGHT][WIDTH];
		/* Set look and feel
		try {
            // Display installed Look & Feels
			//UIManager.LookAndFeelInfo[] lf = UIManager.getInstalledLookAndFeels();
            //for(UIManager.LookAndFeelInfo e : lf){
            //    System.out.println(e.getClassName());
            //}
            
			// DEFAULT
            // UIManager.getCrossPlatformLookAndFeelClassName()
			
			// LINUX, WINDOWS
            // javax.swing.plaf.metal.MetalLookAndFeel
            // com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel
            // com.sun.java.swing.plaf.motif.MotifLookAndFeel
            // com.sun.java.swing.plaf.gtk.GTKLookAndFeel
			
			// WINDOWS
			// com.sun.java.swing.plaf.windows.WindowsLookAndFeel
			// com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel
			
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Exception e){}
		*/
		
		constructGUI();
	}
	
	// Accessor & Mutator Methods
	public JToggleButton[][] getButtons(){
		return toggleButtons;
	}
	
	public void setButtons(JToggleButton[][] bttns){
		toggleButtons = bttns;
	}
	
	public boolean getActionFired(){
		return actionFired;
	}
	
	public void setActionFired(boolean fired){
		actionFired = fired;
	}
	
	public boolean getConnectFired(){
		return connectFired;
	}
	
	public void setConnectFired(boolean fired){
		connectFired = fired;
	}
	
	public int getX(){
		return xCoord;
	}
	
	public void setX(int x){
		xCoord = x;
	}
	
	public int getY(){
		return yCoord;
	}
	
	public void setY(int y){
		yCoord = y;
	}
	
	public String getStatusbar(){
		return statusbar.getText();
	}
	
	public void setStatusbar(String text){
		statusbar.setText(text);
	}
	
	public void addToStatusbar(String text){
		String txt = statusbar.getText();
		if(txt.equals(" ") || txt.equals("")){
			statusbar.setText(text);
		}else{
			statusbar.setText(txt+" | "+text);
		}
	}

	public JFrame getGUI() {
		return frame;
	}

	public Point getGUILocation() {
		return frame.getLocation();
	}

	public Dimension getGUISize() {
		return frame.getSize();
	}

	public void placeGUI(Point loc, Dimension size) {
		frame.setSize(size);
		frame.setLocation(loc);
		//frame.postavi();
	}
	
	// Methods
	public void colorWinRow(char dir, int x, int y){ // kill it, KILL IT WITH FIRE!!!
		String znak = ((ImageIcon)toggleButtons[y][x].getIcon()).getDescription();
		int x_ = x;
		int y_ = y;
		toggleButtons[y][x].setBackground(WIN_COLOR);
		if(dir == 'h'){
			try{
				while(znak.equals(((ImageIcon)toggleButtons[y][x_+1].getIcon()).getDescription())){
					toggleButtons[y][x_+1].setBackground(WIN_COLOR);
					x_++;
				}
			}catch(Exception e){}
			x_ = x;
			try{
				while(znak.equals(((ImageIcon)toggleButtons[y][x_-1].getIcon()).getDescription())){
					toggleButtons[y][x_-1].setBackground(WIN_COLOR);
					x_--;
				}
			}catch(Exception e){}
		}else if(dir == 'v'){
			try{
				while(znak.equals(((ImageIcon)toggleButtons[y_+1][x].getIcon()).getDescription())){
					toggleButtons[y_+1][x].setBackground(WIN_COLOR);
					y_++;
				}
			}catch(Exception e){}
			y_ = y;
			try{
				while(znak.equals(((ImageIcon)toggleButtons[y_-1][x].getIcon()).getDescription())){
					toggleButtons[y_-1][x].setBackground(WIN_COLOR);
					y_--;
				}
			}catch(Exception e){}
		}else if(dir == 'u'){
			try{
				while(znak.equals(((ImageIcon)toggleButtons[y_+1][x_+1].getIcon()).getDescription())){
					toggleButtons[y_+1][x_+1].setBackground(WIN_COLOR);
					x_++;
					y_++;
				}
			}catch(Exception e){}
			x_ = x;
			y_ = y;
			try{
				while(znak.equals(((ImageIcon)toggleButtons[y_-1][x_-1].getIcon()).getDescription())){
					toggleButtons[y_-1][x_-1].setBackground(WIN_COLOR);
					x_--;
					y_--;
				}
			}catch(Exception e){}
		}else if(dir == 'd'){
			try{
				while(znak.equals(((ImageIcon)toggleButtons[y_-1][x_+1].getIcon()).getDescription())){
					toggleButtons[y_-1][x_+1].setBackground(WIN_COLOR);
					x_++;
					y_--;
				}
			}catch(Exception e){}
			x_ = x;
			y_ = y;
			try{
				while(znak.equals(((ImageIcon)toggleButtons[y_+1][x_-1].getIcon()).getDescription())){ //this
					toggleButtons[y_+1][x_-1].setBackground(WIN_COLOR);
					x_--;
					y_++;
				}
			}catch(Exception e){}
		}
	}
	
	private void constructGUI(){
		SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JPanel plosca = new PloscaMain(WIDTH, HEIGHT);
                frame = ustvariOkno("5vVrsto", plosca);
            }
        });
	}
	
	// create a main panel
	private class PloscaMain extends JPanel {
	
        public PloscaMain(int w, int h) {
            setLayout(new GridLayout(h, w, 0, 0));
            for(int i = 0; i < h; i++){
				for(int j = 0; j < w; j++){
					Action a = new CoordAction(j,i);
					
					JToggleButton bttn = new JToggleButton(a);
					//bttn.setBackground(Color.WHITE);
					add(bttn);
					toggleButtons[i][j] = bttn;
				}
			}
			
        }
		
	}
	
	private class CoordAction extends AbstractAction {
	
		public CoordAction(int x, int y){
			super();
			putValue("x", x+1); //retrieve coordinate w/ getValue("x") or ("y")
			putValue("y", y+1); //also, starts cointing from 1, not 0
		}
		
		// action performed when a toggle button is pressed
		public void actionPerformed(ActionEvent e){
			actionFired = true;
			xCoord = (Integer)getValue("x");
			yCoord = (Integer)getValue("y");
			
		}
	
	}
	
	
	// create a JFrame
	private JFrame ustvariOkno(String naslov, JPanel plosca) {
		//mk menubar
		JMenu fileMenu = new JMenu("File");
		JMenu helpMenu = new JMenu("Help");
		
		fileMenu.setMnemonic(KeyEvent.VK_F);
		helpMenu.setMnemonic(KeyEvent.VK_H);
		
		menubar.add(fileMenu);
		menubar.add(helpMenu);
		
		JMenuItem connectAction =   new JMenuItem("Connect");
		JMenuItem quitAction =  new JMenuItem("Quit");
		JMenuItem aboutAction =  new JMenuItem("About");
		
		connectAction.setAccelerator(KeyStroke.getKeyStroke('C', CTRL_DOWN_MASK));
		quitAction.setAccelerator(KeyStroke.getKeyStroke('Q', CTRL_DOWN_MASK));
		
		connectAction.setMnemonic(KeyEvent.VK_C);
		quitAction.setMnemonic(KeyEvent.VK_Q);
		aboutAction.setMnemonic(KeyEvent.VK_A);
		
		fileMenu.add(connectAction);
		fileMenu.add(quitAction);
		helpMenu.add(aboutAction);
		
		connectAction.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0){
				String naslov = "Connect to...";
				String msg = "Input server IP:";
				Icon ico = new ImageIcon("misc/connectIco.png");
				String str = (String)JOptionPane.showInputDialog(null, msg, naslov, JOptionPane.PLAIN_MESSAGE, ico, null, host);
				if( str != null){ //clicked OK
					host = str;
					connectFired = true;
				}else{ /*clicked cancel*/ }
			} 
		});
		
		quitAction.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0){
				System.exit(0);
			} 
		});
		
		aboutAction.addActionListener(new ActionListener()
		{
			public void actionPerformed(ActionEvent arg0){
				String naslov = "About...";
				String msg = "Game: "+IN_A_ROW+" in a row\n";
				msg += "Grid: "+WIDTH+"x"+HEIGHT+"\n";
				if(ALLOW_DISLOCATION){
					msg += "Marks don't have to connect with previous group\n";
				}else{
					msg += "Marks have to connect with previous group\n";
				}
				msg += "\nAuthor: Jurij Slabanja\n";
				msg += "February 2013, v2.2b\n";
				JOptionPane.showMessageDialog(null, msg, naslov, JOptionPane.PLAIN_MESSAGE);
			} 
		});
	
		//mk okno
        Okno okno = new Okno(naslov, WIDTH, HEIGHT, INIT_BTTN_W, INIT_BTTN_H);
        okno.add(plosca, BorderLayout.CENTER);
		okno.add(statusbar, BorderLayout.SOUTH);
		okno.setJMenuBar(menubar);
        okno.pack();
        okno.postavi();
        okno.setVisible(true);
        return okno;
    }
	
	private class Okno extends JFrame {
        private int wZaslon, hZaslon, boardWidth, boardHeight, bttnWidth, bttnHeight;
        private Dimension frameSize;
		private Point frameLoc;

        public Okno(String naslov, int w, int h, int bw, int bh) {
            setTitle(naslov);
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            Toolkit toolkit = Toolkit.getDefaultToolkit();
            Dimension dimZaslon = toolkit.getScreenSize();
            wZaslon = dimZaslon.width;
            hZaslon = dimZaslon.height;
			boardWidth = w;
			boardHeight = h;
			bttnWidth = bw;
			bttnHeight = bh;
			frameSize = new Dimension(boardWidth*bttnWidth+3, boardHeight*bttnHeight+23);
			frameLoc = new Point(wZaslon/8, hZaslon/8);
        }

        public void setSz(Dimension s) {
        	frameSize = s;
        }

		public void setLoc(Point l) {
			frameLoc = l;
		}
		
		public void postavi() {
			setSize(frameSize);
			setLocation(frameLoc);
        }
		
	}
	
}