import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.Scanner;

import java.io.IOException;
import java.net.UnknownHostException;

import java.net.Socket;
import java.net.SocketAddress;
import java.net.InetSocketAddress;
import java.lang.Thread;

import javax.swing.JToggleButton;
import javax.swing.JOptionPane;
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;

import java.awt.Dimension;
import java.awt.Color;
import java.awt.Point;

public class Client extends FiveInARow {
	private static ClientGUI gui = null;

	public static void main(String[] args) {
		boolean connectfired = false;

		showConnectDialog(); // now host & port is set properly
		while (true) {
			game();
			connectfired = gui.getConnectFired();
			while(!connectfired){
				connectfired = gui.getConnectFired();
				try{
					Thread.currentThread().sleep(5); // has to pause for a bit or the while doesn't exit
				}catch(InterruptedException ie){}	
			}
			gui.setConnectFired(false);
		}
			
	}
	
	private static void game() {
		int pTurn = 0;
		int pID = 0;
		int numWon = 0;
		int numGames = 0;
		String endCondition = "";
		Color bttnBackDefault = null;
		DataOutputStream out = null;
		DataInputStream in = null;
		JToggleButton[][] tBttns = null;
		boolean cont = false;
		String response = "";
		Dimension guiSize = null;
		Point guiLoc = null;
		boolean placeInDefaultPos = true;


		System.out.println("Connecting...");
		Socket server = connect(host, port); // connect
		try {
			out = new DataOutputStream(server.getOutputStream());
			in = new DataInputStream(server.getInputStream());
		} catch(Exception e) { System.out.println("Failed to open data streams to server."); System.exit(1); }

		do {
			// GAME PREP
			if (!cont) { // we aren't continuing, therefore it's a new game
				System.out.println("Getting config...");
				try {
					response = in.readUTF();
				} catch(Exception e) { System.out.println("Failed to read confing from server."); System.exit(1); }

				String[] conf = response.split(" ");
				HEIGHT = Integer.parseInt(conf[0]);
				WIDTH = Integer.parseInt(conf[1]);
				IN_A_ROW = Integer.parseInt(conf[2]);
				if (conf[3].equals("false"))
					ALLOW_DISLOCATION = false;
				else
					ALLOW_DISLOCATION = true;
				NUM_PLAYERS = Integer.parseInt(conf[4]);
				pID =  Integer.parseInt(conf[5]);

				System.out.println("Building GUI...");

				if (gui != null) {// delete gui
					guiLoc = gui.getGUILocation();
					guiSize = gui.getGUISize();
					placeInDefaultPos = false;
					gui.getGUI().dispose();
				}
				gui = new ClientGUI(); // mk gui
				if (!placeInDefaultPos) {
					try{ // TODO: this is really not kosher, fix this concurrency issue (error get null ptr in ClientGUI public void placeGUI(Point loc, Dimension size) ... frame = null)
						Thread.currentThread().sleep(100); 
					}catch(InterruptedException ie){} 
					gui.placeGUI(guiLoc, guiSize);
				}
				//System.out.println("Done.");
			}

			System.out.println("Waiting for other players...");

			try { // get player ID & initial board layout
				response = in.readUTF();
			} catch(Exception e) { System.out.println("Failed to read player turn# & initial board layout."); System.exit(1); }

			Scanner scnP_B = new Scanner(response);
			pTurn = scnP_B.nextInt(); // player turn

			tBttns = gui.getButtons();
			try{ // TODO: this is really not kosher, fix this concurrency issue (error get in parseBoard when trying to use tBttns; tBttns=null)
				Thread.currentThread().sleep(500); // apparently the initial setting of the buttons takes some time to get done
			}catch(InterruptedException ie){} 

			bttnBackDefault = tBttns[0][0].getBackground();
			String boardAsLine = scnP_B.next();
			tBttns = parseBoard(boardAsLine, tBttns, bttnBackDefault);

			gui.setButtons(tBttns); // board layout
			gui.setStatusbar(PLAYER_ID[pID]+"|Connected with "+server.getInetAddress().toString()+", you are player #"+(pTurn+1));

			// GAME MAIN
			System.out.println("Game on!!");
			// get the board layout for each player that is before you
			try {
				for(int i = 0; i < pTurn; i ++){
					response = in.readUTF();
					tBttns = parseBoard(response, tBttns, bttnBackDefault);
					gui.setButtons(tBttns);
					response = in.readUTF(); // reads the win/lose condition - first round can only be normal
				}
			} catch(Exception e) { System.out.println("Failed to read other players' board layouts and/or game statuses."); System.exit(1); }

			gui.setActionFired(false);
			endCondition = "";
			while(endCondition.equals("")){
				gui.addToStatusbar("Your turn");
				// get coordinates when an action is fired
				boolean actionFired = gui.getActionFired();
				while(!actionFired){
					actionFired = gui.getActionFired();
					try{
						Thread.currentThread().sleep(5); // hrequires a short pause or the while doesn't exit
					}catch(InterruptedException ie){}	
				}
				int xCoord = gui.getX(); // TODO: teoreticno, ce bi bil dost hiter bi lahk vecgumbov naenkrat prtisnu...
				int yCoord = gui.getY();	

				try { // publish your coordinates to server & get a response
					out.writeUTF(xCoord+" "+yCoord); //format: "2 3" -> coords x:2 y:3
				} catch(Exception e) { System.out.println("Failed to send coords to server."); System.exit(1); }

				try {
					response = in.readUTF(); //response is either "error #" or "no error"
				} catch(Exception e) { System.out.println("Failed to read error status from server."); System.exit(1); }
				
				if((response.substring(0,5)).equals("error")){
					Scanner scn = new Scanner(response);
					scn.next();
					int errorNum = scn.nextInt();
					switch (errorNum){
						case 1: gui.setStatusbar(PLAYER_ID[pID]+"|Error: Mark has to connect with previous group");
							break;
						case 2: gui.setStatusbar(PLAYER_ID[pID]+"|Error: Mark already exists at this location");
							break;
						//case 0: gui.setStatusbar("No jumping in line!"); // technically can't happen & currently isn't implemented
						//	break;
					}
					tBttns[yCoord-1][xCoord-1].setSelected(false);
					gui.setActionFired(false); // you don't want the same button constantly firing
					gui.setButtons(tBttns);
					continue; //deselect button & retry
				}
				
				// if you got to here the coordinates were ok - get board layout after each player's move
				gui.setStatusbar(PLAYER_ID[pID]+" ");
				for(int i = 0; i < NUM_PLAYERS; i++){
					try {
						response = in.readUTF();
					} catch(Exception e) { System.out.println("Failed to read board layout from server."); System.exit(1); }
					tBttns = parseBoard(response, tBttns, bttnBackDefault);
					gui.setButtons(tBttns);
					
					try { //check for win/lose (if neither response = "normal" or "full" if no moves left)
						response = in.readUTF();
					} catch(Exception e) { System.out.println("Failed to read game status from server."); System.exit(1); }

					if((response.substring(0,3)).equals("win")){ // if win you get "win d" where d is the direction of win row(single char)
						if (response.charAt(4) == 'a'){
							gui.setStatusbar("You won! Everybody else has disconnected.");
						} else {
							gui.setStatusbar("You won!");
							gui.colorWinRow(response.charAt(4), xCoord-1, yCoord-1); // these coords start counting at 1 (want at 0)
						}
						gui.setConnectFired(false); // apparently this needs to be here
						endCondition = "Won";
						numWon++;
						break;
					}else if(response.equals("lose")){
						gui.setStatusbar("You lost :(");
						endCondition = "Lost";
						break;
					}else if(response.equals("full")){
						gui.setStatusbar("Tie");
						endCondition = "Tie";
						break;
					}
				}
				gui.setActionFired(false);
			}
			numGames++;
			cont = (JOptionPane.showConfirmDialog(null, "Play again?\nWon: "+numWon+"/"+numGames, endCondition, JOptionPane.YES_NO_OPTION) != JOptionPane.NO_OPTION);
			try { 
				out.writeBoolean(cont);
				if (cont) // if you didn't want to continue you don't have to get continuation status from server
					cont = in.readBoolean();
			} catch(Exception e) { System.out.println("Failed to read/write game continuation status."); System.exit(1); }
		} while(cont);
		try { 
			server.close();
		} catch(Exception e) { System.out.println("Failed to close connection to server."); System.exit(1); }
	}

	private static Socket connect(String host, int port) {
		try {
			SocketAddress serveraddr = new InetSocketAddress(host, port);
			Socket server = new Socket();
			server.setSoTimeout(0);
			server.connect(serveraddr, 3600000); // timeout = 1h
			server.setSoTimeout(0);
			return server;
		} catch (UnknownHostException uhe) {
			System.out.println("The server host address("+host+")is unknown.");
			uhe.printStackTrace();
		} catch (IOException ioe) {
			System.out.println("IOException occured when connecting to server: " + ioe);
			ioe.printStackTrace();
			System.exit(1);
		}
		
		return null;
	}
	
	private static JToggleButton[][] parseBoard(String boardAsLine, JToggleButton[][] tBttns, Color bttnBackDefault){
		for(int h = 0; h < HEIGHT; h++){ // parse the borad layout you got as a 2D array
			for(int w = 0; w < WIDTH; w++){
				String znak = ""+boardAsLine.charAt(WIDTH*h+w);
				tBttns[h][w].setSelected(false);
				if(znak.equals(".")){
					tBttns[h][w].setIcon(null);
					tBttns[h][w].setBackground(bttnBackDefault);
				}else{
					
					ImageIcon icon = new ImageIcon("marks/"+znak+".png"); //scale icon to initial button size (at least try)
					Image img = icon.getImage() ;  
					Image newimg = img.getScaledInstance( INIT_BTTN_W, INIT_BTTN_H, Image.SCALE_SMOOTH ) ;  
					icon = new ImageIcon( newimg );
					icon.setDescription(znak);

					tBttns[h][w].setIcon(icon);
					tBttns[h][w].setBackground(Color.WHITE);

				}
			}
		}	
		return tBttns;
	}
	

	private static void showConnectDialog() {
		String naslov = "Connect to...";
		String msg = "Input server IP:";
		Icon ico = new ImageIcon("misc/connectIco.png");
		String str = (String)JOptionPane.showInputDialog(null, msg, naslov, JOptionPane.PLAIN_MESSAGE, ico, null, host);
		if( str != null){ //clicked OK
			host = str;
		}else{ /*clicked cancel*/ }
	} 
}